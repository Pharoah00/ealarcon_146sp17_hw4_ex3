import java.util.Scanner;

/**
 * @author Eduardo Alarcon
 * @version Homework 4 Exercise 3: Improving the "Name Parser" App
 */
public class ImproveNameParserApp 
{

	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter a name: ");
		String name = sc.nextLine();
		System.out.println();
		name = name.trim();

		// Bird is the Word
		String[] arrayName = name.split(" ");

		if ( arrayName.length < 2 ) 
		{
			System.err.println("Name not in valid format.");
		} 
		else 
		{
			for( int i = 0; i < arrayName.length; i++)
	        {
				
				if (i == 0) 
				{
					System.out.println("First name:  " + arrayName[i]);
					                
				}
				else if( i == arrayName.length - 1 )
				{
					System.out.println("Last name: " + arrayName[i]);
				} //end else-if
				else 
				{
					System.out.println("Middle name" + i + ":  " + arrayName[i]);
					
				} //end nested if-block
	        } //end for-loop
		} //end if-else block

		sc.close();

	} //end main method

} //end class ImprovedNameParserApp


// Copied Solution from Exercise 9-1 from 
// Murach's Beginning Java with Eclipse

/*package murach.name;

import java.util.Scanner;

public class NameParserApp {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter a name: ");
        String name = sc.nextLine();
        System.out.println();
        name = name.trim();

        int index1 = name.indexOf(" ");
        if (index1 == -1) {
            System.out.println("Name not in valid format.");
        } else {
            int index2 = name.indexOf(" ", index1 + 1);
            if (index2 == -1) {
                String firstName = name.substring(0, index1);
                String lastName = name.substring(index1 + 1);
                System.out.println("First name:  " + firstName);
                System.out.println("Last name:   " + lastName);                
            } else {
                String firstName = name.substring(0, index1);
                String middleName = name.substring(index1 + 1, index2);
                String lastName = name.substring(index2 + 1);

                System.out.println("First name:   " + firstName);
                System.out.println("Middle name:  " + middleName);
                System.out.println("Last name:    " + lastName);     
            }
        }
        sc.close();
    }
}*/